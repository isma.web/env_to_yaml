mod config_item;
mod patch_doc;

use std::env;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

use yaml_rust::{YamlEmitter, YamlLoader};

fn main() {
    // take 2 arguments, first is the template, second is the output file
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        panic!("Expected 2 arguments, got {}", args.len() - 1);
    }
    let template_path = Path::new(&args[1]);
    let output_path = Path::new(&args[2]);

    // Read the YAML template file
    // read text file
    println!("Reading template file: {:?}", template_path);
    let text = {
        let mut template = File::open(template_path).expect("Could not open template for reading");
        let mut text = String::new();
        template
            .read_to_string(&mut text)
            .expect("Failed to read file");
        text
    };

    let yaml = YamlLoader::load_from_str(text.as_str()).expect("Failed to parse YAML file");
    let mut doc = if yaml.len() != 1 {
        panic!("Expected exactly one YAML document");
    } else {
        yaml.into_iter().next().unwrap()
    };

    // collect all environment variables starting with "CFG_", strip the prefix and make a map
    let cfg = env::vars()
        .filter(|(k, _)| k.starts_with("CFG_"))
        .map(|(k, v)| (k[4..].to_lowercase(), v));

    // parse config rules
    let config_items = patch_doc::parse_config_items(cfg).expect("Failed to parse config items");

    if config_items.is_empty() {
        println!("No environment variables starting with CFG_ found");
    }

    patch_doc::patch_doc(&mut doc, config_items.into_iter()).expect("Failed to patch document");

    let text = {
        let mut buffer = String::new();
        let mut emitter = YamlEmitter::new(&mut buffer);
        emitter.dump(&doc).expect("Failed to serialize YAML");
        buffer
    };

    // write the YAML document to the output file
    println!("Writing output file: {:?}", output_path);
    let mut output = File::create(output_path).expect("Could not open output file for writing");
    output
        .write_all(text.as_bytes())
        .expect("Failed to write file");
}
