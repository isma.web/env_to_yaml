use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidFragment(String),
    InvalidArrayIndex(usize),
    MismatchTypeExpectedHash(),
    MismatchTypeExpectedArray(),
}

type Index = usize;

#[derive(Debug, PartialEq)]
pub enum PathFragment {
    Hash(String), // >item1<_item1.1 = value
    Array(Index), // item2>[0]<_item2.1 = value
}

pub fn display_path(path: &[PathFragment]) -> String {
    path.iter()
        .map(PathFragment::to_string)
        .collect::<Vec<_>>()
        .join("/")
}

impl Display for PathFragment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PathFragment::Hash(s) => write!(f, "{}", s),
            PathFragment::Array(i) => write!(f, "{}", i),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct ConfigItem {
    pub path: Vec<PathFragment>,
    pub value: String,
}

impl ConfigItem {
    pub fn new(path: String, value: String) -> Result<ConfigItem, Error> {
        let path: Vec<_> = path
            .replace('[', "_")
            .replace(']', "") // item[0] to item_0
            .split('_')
            .map(Self::parse_fragment)
            .collect::<Result<_, _>>()?;
        Ok(ConfigItem { path, value })
    }

    fn parse_fragment(fragment: &str) -> Result<PathFragment, Error> {
        if Self::is_number(fragment) {
            let idx = fragment
                .parse()
                .map_err(|_| Error::InvalidFragment(fragment.to_string()))?;
            Ok(PathFragment::Array(idx))
        } else {
            Ok(PathFragment::Hash(fragment.to_lowercase()))
        }
    }

    fn is_number(s: &str) -> bool {
        s.chars().all(|c| c.is_ascii_digit())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_fragment() {
        assert_eq!(
            ConfigItem::parse_fragment("ITEM1"),
            Ok(PathFragment::Hash("item1".to_string()))
        );
        assert_eq!(ConfigItem::parse_fragment("0"), Ok(PathFragment::Array(0)));
        assert_eq!(ConfigItem::parse_fragment("1"), Ok(PathFragment::Array(1)));
    }

    #[test]
    fn test_parse_path() {
        assert_eq!(
            ConfigItem::new("ITEM1_ITEM1.1".to_string(), "value".to_string()),
            Ok(ConfigItem {
                path: vec![
                    PathFragment::Hash("item1".to_string()),
                    PathFragment::Hash("item1.1".to_string()),
                ],
                value: "value".to_string(),
            })
        );
        assert_eq!(
            ConfigItem::new("ITEM1_ITEM1.1[0]".to_string(), "value".to_string()),
            Ok(ConfigItem {
                path: vec![
                    PathFragment::Hash("item1".to_string()),
                    PathFragment::Hash("item1.1".to_string()),
                    PathFragment::Array(0),
                ],
                value: "value".to_string(),
            })
        );
        assert_eq!(
            ConfigItem::new("ITEM1_ITEM1.1[0]_a".to_string(), "value".to_string()),
            Ok(ConfigItem {
                path: vec![
                    PathFragment::Hash("item1".to_string()),
                    PathFragment::Hash("item1.1".to_string()),
                    PathFragment::Array(0),
                    PathFragment::Hash("a".to_string())
                ],
                value: "value".to_string(),
            })
        );
    }
}
