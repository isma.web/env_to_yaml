use std::iter::Peekable;
use std::vec::IntoIter;

use crate::config_item::{display_path, ConfigItem, Error, PathFragment};
use yaml_rust::Yaml;

pub fn parse_config_items(
    cfg: impl Iterator<Item = (String, String)>,
) -> Result<Vec<ConfigItem>, Error> {
    cfg.map(|(k, v)| ConfigItem::new(k, v))
        .collect::<Result<_, _>>()
}

pub fn patch_doc(doc: &mut Yaml, cfg: impl Iterator<Item = ConfigItem>) -> Result<(), Error> {
    for ConfigItem { path, value: v } in cfg {
        println!("{}: {}", display_path(&path), v);
        recursive_patch(doc, &mut path.into_iter().peekable(), v.to_string())?;
    }
    Ok(())
}

fn recursive_patch(
    current: &mut Yaml,
    path_iter: &mut Peekable<IntoIter<PathFragment>>,
    v: String,
) -> Result<(), Error> {
    let fragment = path_iter.next();
    match fragment {
        None => return Ok(()),
        Some(PathFragment::Hash(key)) => {
            let key = Yaml::String(key);
            let hash = if let Yaml::Hash(hash) = current {
                hash
            } else {
                return Err(Error::MismatchTypeExpectedHash());
            };
            if let Some(next_fragment) = path_iter.peek() {
                if let Some(next) = hash.get_mut(&key) {
                    return recursive_patch(next, path_iter, v);
                }

                let mut next = match next_fragment {
                    PathFragment::Hash(_) => Yaml::Hash(Default::default()),
                    PathFragment::Array(_) => Yaml::Array(Default::default()),
                };
                recursive_patch(&mut next, path_iter, v)?;
                hash.insert(key, next);
                return Ok(());
            }
            hash.insert(key, Yaml::String(v));
        }
        Some(PathFragment::Array(idx)) => {
            let arr = if let Yaml::Array(arr) = current {
                arr
            } else {
                return Err(Error::MismatchTypeExpectedArray());
            };
            if let Some(next) = arr.get_mut(idx) {
                return recursive_patch(next, path_iter, v);
            }
            if let Some(next_fragment) = path_iter.peek() {
                let mut next = match next_fragment {
                    PathFragment::Hash(_) => Yaml::Hash(Default::default()),
                    PathFragment::Array(_) => Yaml::Array(Default::default()),
                };
                recursive_patch(&mut next, path_iter, v)?;
                arr.push(next);
                return Ok(());
            }
            match arr.len() {
                len if len == idx => arr.push(Yaml::String(v)),
                len if len > idx => arr[idx] = Yaml::String(v),
                _ => return Err(Error::InvalidArrayIndex(idx)),
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod test {
    use yaml_rust::YamlLoader;

    use super::*;

    #[test]
    fn test_empty() {
        let cfg = parse_config_items(vec![].into_iter()).unwrap();
        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();
        assert_eq!(doc, Yaml::Hash(Default::default()));
    }

    #[test]
    fn test_single_item_depth_1() {
        let items = vec![("ITEM1".to_string(), "value".to_string())];
        let cfg = parse_config_items(items.into_iter()).unwrap();
        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();
        assert_eq!(doc["item1"].as_str().unwrap(), "value".to_string());
    }

    #[test]
    fn test_single_item_depth_1_existing() {
        let items = vec![("ITEM1".to_string(), "value".to_string())];
        let cfg = parse_config_items(items.into_iter()).unwrap();
        let mut doc = YamlLoader::load_from_str("item1: old value")
            .unwrap()
            .pop()
            .unwrap();

        println!("{:?}", doc);

        patch_doc(&mut doc, cfg.into_iter()).unwrap();

        println!("{:?}", doc);

        assert_eq!(doc["item1"].as_str().unwrap(), "value".to_string());
    }

    #[test]
    fn test_single_item_depth_2() {
        let items = vec![("ITEM1_ITEM1.1".to_string(), "value".to_string())];
        let cfg = parse_config_items(items.into_iter()).unwrap();
        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();
        assert_eq!(
            doc["item1"]["item1.1"].as_str().unwrap(),
            "value".to_string()
        );
    }

    #[test]
    fn test_items_depth_2() {
        let items = vec![
            ("ITEM1_ITEM1.1".to_string(), "value1".to_string()),
            ("ITEM1_ITEM1.2".to_string(), "value2".to_string()),
        ];
        let cfg = parse_config_items(items.into_iter()).unwrap();

        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();

        assert_eq!(doc["item1"]["item1.1"].as_str().unwrap(), "value1");
        assert_eq!(doc["item1"]["item1.2"].as_str().unwrap(), "value2");
    }

    #[test]
    fn test_items_depth_2_arr() {
        let items = vec![
            ("ITEM1_ITEM1.1".to_string(), "value1".to_string()),
            ("ITEM1_ITEM1.2[0]".to_string(), "value2".to_string()),
        ];
        let cfg = parse_config_items(items.into_iter()).unwrap();

        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();

        assert_eq!(doc["item1"]["item1.1"].as_str().unwrap(), "value1");
        assert_eq!(doc["item1"]["item1.2"][0].as_str().unwrap(), "value2");
    }

    #[test]
    fn test_items_depth_2_arr_hash() {
        let items = vec![
            ("ITEM1_ITEM1.1".to_string(), "value1".to_string()),
            ("ITEM1_ITEM1.2[0]_a".to_string(), "value2".to_string()),
            ("ITEM1_ITEM1.2[0]_b".to_string(), "value3".to_string()),
        ];
        let cfg = parse_config_items(items.into_iter()).unwrap();

        let mut doc = Yaml::Hash(Default::default());
        patch_doc(&mut doc, cfg.into_iter()).unwrap();

        assert_eq!(doc["item1"]["item1.1"].as_str().unwrap(), "value1");
        assert_eq!(doc["item1"]["item1.2"][0]["a"].as_str().unwrap(), "value2");
        assert_eq!(doc["item1"]["item1.2"][0]["b"].as_str().unwrap(), "value3");
    }
}
